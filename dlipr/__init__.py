__all__ = [
    "utils",
    "cifar",
    "mnist",
    "svhn",
    "ising",
    "flower",
    "speckles"]

import dlipr.utils
import dlipr.cifar
import dlipr.mnist
import dlipr.svhn
import dlipr.ising
import dlipr.flower
import dlipr.speckles
import tensorflow as tf

class GPUError(Exception):
    pass

gpus = tf.config.experimental.list_physical_devices('GPU')
memory_limit = 8000
if gpus:
    try:
        tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=memory_limit)])
    except RuntimeError as e:
        print(e)
else:
    raise GPUError("No GPU was found, please submit using 'pygpu'")

